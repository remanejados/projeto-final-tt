import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  apiUrl: string = "http://localhost:8000/api/";

  constructor(public http: HttpClient) { }

  httpHeaders: any = {
    headers: {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    }
  }

  showUser(id): Observable<any>{
    return this.http.get(this.apiUrl + 'user/'+ id, this.httpHeaders)
  }


  getDetails(): Observable<any>{
     this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
     return this.http.get(this.apiUrl + 'getDetails', this.httpHeaders)
  }

  editProfile(form): Observable<any>{
    console.log(form);

    this.httpHeaders.headers['Authorization']= 'Bearer ' + localStorage.getItem('userToken')
    return this.http.put(this.apiUrl + 'user', form, this.httpHeaders)
  }

  friendList(): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.get(this.apiUrl + 'friend_list', this.httpHeaders)
  }

  isFollowing(id): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.get(this.apiUrl + 'is_following/' + id, this.httpHeaders)
  }

  follow(id): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.post(this.apiUrl + 'user/follow/' + id, null, this.httpHeaders)
  }

  getFeed(): Observable<any>{
    this.httpHeaders.headers['Authorization'] = 'Bearer ' + localStorage.getItem('userToken');
    return this.http.get(this.apiUrl + 'feed', this.httpHeaders)
  }

  getUserReviews(id): Observable<any> {
    return this.http.get(this.apiUrl + 'user/reviews/' + id, this.httpHeaders);
  }
}
