import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { CommentService } from '../services/comment/comment.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-edit-comment',
  templateUrl: './edit-comment.page.html',
  styleUrls: ['./edit-comment.page.scss'],
})
export class EditCommentPage implements OnInit {
  updateCommentForm: FormGroup;
  id:number;
  comment;

  constructor(private router: Router, public formbuilder: FormBuilder, public toastCtrl: ToastController, private commentService: CommentService, private activatedRoute: ActivatedRoute) {
    this.updateCommentForm = this.formbuilder.group({
      text: [null, [Validators.required, Validators.maxLength(140)]],
    });

    this.activatedRoute.params.subscribe(params => {
      this.id = params.id;
    })
  }

  ngOnInit() {
    this.getComment();
  }

  logRatingChange(rating){
    console.log("changed rating: ",rating);
  }

  async openToast() {
    const toast = await this.toastCtrl.create({
      message: 'Review enviado',
      duration: 4000,
      position: 'middle',
      color: 'roxinho',
    });
    toast.present();
  }

  updateComment(){
    this.updateCommentForm.value.review_id = this.comment.review_id;
    this.commentService.updateComment(this.id, this.updateCommentForm.value).subscribe(
      (res) => {
        console.log('pêra', res);
        this.router.navigate(['/comments/' + this.comment.review_id]).then(()=> {window.location.reload()});
      }
    )
  }

  getComment(){
    this.commentService.getComment(this.id).subscribe(
      (res) => {
        this.comment = res.comment;
        console.log('comment', this.comment);
      })
  }
}
