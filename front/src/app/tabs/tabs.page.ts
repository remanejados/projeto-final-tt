import { Component } from '@angular/core';
import { UserService } from '../services/user/user.service'
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  user;

  constructor(private router: Router, private userService: UserService) {
    this.getDetails();
  }

  getDetails(){
    if (localStorage.getItem('userToken')){
      this.userService.getDetails().subscribe(
        (res) => {
          this.user = res;
          console.log( 'acabate', this.user);
        }
      )
    }
  }

  go(){
    this.router.navigate(['/tabs/tab3/' + this.user.user.id]).then(()=> {window.location.reload()})
  }
}
