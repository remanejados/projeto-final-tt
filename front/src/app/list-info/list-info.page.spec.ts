import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ListInfoPage } from './list-info.page';

describe('ListInfoPage', () => {
  let component: ListInfoPage;
  let fixture: ComponentFixture<ListInfoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListInfoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ListInfoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
