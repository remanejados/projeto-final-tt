import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';


export class Lists{
  name:string;
  description: string;
  photo: string;
}

@Component({
  selector: 'app-list-info',
  templateUrl: './list-info.page.html',
  styleUrls: ['./list-info.page.scss'],
})
export class ListInfoPage implements OnInit {

  lists: Lists[];

  constructor() {}


 ngOnInit() {
    this.lists = [
      {
        name: 'ok',
        description: 'ok',
        photo: 'https://i.pinimg.com/originals/c2/a1/7f/c2a17f807850531fec1606ab4444ebea.jpg'
      },
      {
        name: 'ok',
        description: 'ok',
        photo: 'https://i.pinimg.com/originals/c2/a1/7f/c2a17f807850531fec1606ab4444ebea.jpg'
      },
      {
        name: 'ok',
        description: 'ok',
        photo: 'https://i.pinimg.com/originals/c2/a1/7f/c2a17f807850531fec1606ab4444ebea.jpg'
      },
      {
        name: 'ok',
        description: 'ok',
        photo: 'https://i.pinimg.com/originals/c2/a1/7f/c2a17f807850531fec1606ab4444ebea.jpg'
      },
      {
        name: 'ok',
        description: 'ok',
        photo: 'https://i.pinimg.com/originals/c2/a1/7f/c2a17f807850531fec1606ab4444ebea.jpg'
      },
    ]
  }
}
