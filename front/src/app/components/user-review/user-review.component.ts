import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-review',
  templateUrl: './user-review.component.html',
  styleUrls: ['./user-review.component.scss'],
})
export class UserReviewComponent implements OnInit {
  @Input() review;

  id:number;
  reviews;

  constructor(private router: Router) {
  }

  ngOnInit() {
    console.log('banana', this.review);
  }

  go(){
    let id = this.review.id;
    this.router.navigate(['/film-review/' + id]);
  }
}
