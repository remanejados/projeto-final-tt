<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Http\Request;
use Laravel\Passport\HasApiTokens;


class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    public function createUser(Request $request){
        $this->email = $request->email;
        $this->name = $request->name;
        if($request->is_admin){
            $this->is_admin = $request->is_admin;
        }
        $this->password = bcrypt($request->password);
        if($request->user_avatar){
            $this->user_avatar = $request->user_avatar;
        }
        if($request->user_cover){
            $this->user_cover = $request->user_cover;
        }
        $this->user_name = $request->user_name;
        $this->save();
    }

    public function updateUser (Request $request){
        if($request->email){
            $this->email = $request->email;
        }
        if($request->is_admin){
            $this->is_admin = $request->is_admin;
        }
        if($request->name){
            $this->name = $request->name;
        }
        if($request->password){
            $this->password = bcrypt($request->password);
        }
        if($request->user_avatar){
            $this->user_avatar = $request->user_avatar;
        }
        if($request->user_cover){
            $this->user_cover = $request->user_cover;
        }
        if($request->user_name){
            $this->user_name = $request->user_name;
        }
        $this->save();
    }
    public function reviews(){
        return $this->hasMany('App\Models\Review');
    }

    public function comments(){
        return $this->hasMany('App\Models\Comment');
    }

    public function follow(){
        return $this->belongsToMany('App\Models\User', 'follows', 'follow_id','followed_id');
    }

    public function followed(){
        return $this->belongsToMany('App\Models\User', 'follows', 'followed_id','follow_id');
    }

    public function likeFilm(){
        return $this->belongsToMany('App\Models\Film', 'likesfilms', 'user_id', 'film_id');
    }

    public function likeReview(){
        return $this->belongsToMany('App\Models\Review', 'likesreviews', 'user_id', 'review_id');
    }

    public function followUser($id){
        $user = User::find($id);
        $this->follow()->attach($user);
    }

    public function unfollowUser($id){
        $user = User::find($id);
        $this->follow()->detach($user);
    }

    public function doLikeFilm($id){
        $film = Film::find($id);
        $this->likeFilm()->attach($film);
    }

    public function doUnlikeFilm($id){
        $film = Film::find($id);
        $this->likeFilm()->detach($film);
    }

    public function doLikeReview($id){
        $review = Review::find($id);
        $this->likeReview()->attach($review);
    }

    public function doUnlikeReview($id){
        $review = Review::find($id);
        $this->likeReview()->detach($review);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
