<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Review extends Model
{
    use HasFactory;
    public function createReview($request,$user_id){
        $this->film_id = $request->film_id;
        $this->film_rating = $request->film_rating;
        $this->text = $request->text;
        $this->user_id = $user_id;
        $this->save();
    }

    public function updateReview ($request){
        if($request->film_rating){
            $this->film_rating = $request->film_rating;
        }
        if($request->text){
            $this->text = $request->text;
        }
        $this->save();
    }

    // public function user()
    // {
    //     return $this->belongsTo('App\Models\User');
    // }

    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    // public function qualquernome()
    // {
    //     return $this->belongsTo('App\Models\User')->select('id','user_name','user_avatar');
    // }
    public function film()
    {
        return $this->belongsTo('App\Models\Film');
    }
    public function comment()
    {
        return $this->hasMany('App\Models\Comment');
    }

    public function setUser($user_id)
    {
        $user = User::find($user_id);
        $this->user_id = $user_id;
        $this->save();
    }
    public function setFilm($film_id)
    {
        return $film_id;
        $film = Film::find($film_id);
        $this->film_id = $film_id;
        $this->save();
    }
}
