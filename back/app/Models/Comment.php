<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Comment extends Model
{
    use HasFactory;
    public function createComment($request,$user_id){
        $this->review_id = $request->review_id;
        $this->text = $request->text;
        $this->user_id = $user_id;
        $this->save();
    }

    public function updateComment ($request){
        if($request->text){
            $this->text = $request->text;
        }
        $this->save();
    }

    public function review()
    {
        return $this->belongsTo('App\Models\Review');
    }
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
}
