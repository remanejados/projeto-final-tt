<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Film extends Model
{
    use HasFactory;
    public function createFilm($request){
        $this->age_rating = $request->age_rating;
        $this->director = $request->director;
        $this->genre = $request->genre;
        $this->length = $request->length;
        $this->film_cover = $request->film_cover;
        $this->film_rating = $request->film_rating;
        $this->film_scene = $request->film_scene;
        $this->release_date = $request->release_date;
        $this->resume = $request->resume;
        $this->title = $request->title;
        $this->save();
    }
    public function updateFilm ($request){
        if($request->age_rating){
            $this->age_rating = $request->age_rating;
        }
        if($request->director){
            $this->director = $request->director;
        }
        if($request->genre){
            $this->genre = $request->genre;
        }
        if($request->length){
            $this->length = $request->length;
        }
        if($request->film_rating){
            $this->film_rating = $request->film_rating;
        }
        if($request->release_date){
            $this->release_date = $request->release_date;
        }
        if($request->resume){
            $this->resume = $request->resume;
        }
        if($request->title){
            $this->title = $request->title;
        }
        if($request->film_cover){
            $this->film_cover = $request->film_cover;
        }
        if ($request->filme_scene){
            $this->film_scene = $request->film_scene;
        }
        $this->save();
    }

    public function reviews(){
        return $this->hasMany('App\Models\Review');
    }

    public function liked(){
        return $this->belongsToMany('App\Models\User', 'likesfilms', 'film_id', 'user_id');
    }
}
