<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Review;

class ReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')){
            return [
                'film_rating' => 'required|numeric',
                'text' => 'required|string', 
            ];
        }
        if ($this->isMethod('PUT')){
            return [
                'film_rating' => 'numeric',
                'text' => 'string'
            ];
        }
    }
    public function message()
    {
        return [
            'film_rating.required' => 'este atributo não pode ser nulo', 
            'text.required' => 'este atributo não pode ser nulo'
        ];
    }
    protected function failedValidation(Validator $validator){
        throw new HttpResponseException(response()->json($validator->errors(), 422));
    }
}
