<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\User;


class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->isMethod('POST')){
            return [
                'user_avatar' => 'string',
                'email' => 'required|email|unique:Users,email',
                'is_admin' => 'boolean|digits:1',
                'name' => 'string',
                'password' => 'required|string|min:6|max:15',
                'user_cover' => 'string',
                'user_name' => 'required|string|unique:Users,user_name|min:3|max:15'
            ];
        }
        if ($this->isMethod('PUT')){
            return [
                'user_avatar' => 'nullable|string', 
                'email' => 'nullable|email|unique:Users,email',
                'is_admin' => 'nullable|boolean|digits:1',
                'name' => 'nullable|string',
                'password' => 'nullable|string',
                'user_cover' => 'nullable|string',
                'user_name' => 'nullable|string|unique:Users,user_name'
            ];
        }

    }

    public function messages()
    {
        return [
            'email.required' => 'O email não pode ser nulo',
            'email.email' => 'Formato de e-mail errado',
            'email.unique' => 'Este e-mail já está cadastrado',
            'is_admin.boolean' =>  'O atributo is_admin deve ter um valor booleano',
            'is_admin.digits' =>  'O atributo is_admin deve ter apenas um digito',
            'password.required' => 'A senha não pode ser nula',
            'user_name.required' => 'O nome de usuário não pode ser nulo',
            'user_name.unique' => 'Este nome de usuário já está sendo utilizado',
            'user_name.min' => 'O nome do usuario deve ter entre 3 e 15 caracteres',
            'user_name.max' => 'O nome do usuario deve ter entre 3 e 15 caracteres'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json($validator->errors(),422));
    }

}
