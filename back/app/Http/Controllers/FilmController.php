<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\FilmRequest;
use App\Models\Film;
use DB;
use Auth;
use App\Models\Review;
use App\Http\Resources\ReviewResource;
use App\Models\User;

class FilmController extends Controller
{
    //
    public function create(FilmRequest $request)
    {
        $film = new Film();
        $film->createFilm($request);
        return response()->json(['film'=> $film], 200);
    }
    public function update (FilmRequest $request, $id){
        $film = Film::find($id);
        $film->updateFilm($request);
        return response()->json(['film' => $film], 200);
    }
    public function index(){
        $films = Film::all();
        return response()->json(['film' => $films],200);
    }

    public function show($id){
        $film = Film::find($id);
        return response()->json(['film'=> $film], 200);
    }

    public function destroy($id){
        $film = Film::find($id);
        $film->delete();
        return response()->json(['Filme deletado com sucesso'],200);
    }

    public function searchByName(Request $request){
        $film = DB::table('films')->where('title','like', '%'.$request->title.'%')->get();
        return  response()->json(['title' => $film],200);   
    }

    public function searchBothByName(Request $request){
        $film = Film::where('title','like', '%'.$request->search.'%')->get(['id','title','director','film_cover','release_date']);
        $film_director = Film::where('director','like', '%'.$request->search.'%')->get(['id','title','director','film_cover','release_date']);
        $film = $film->concat($film_director)->unique();
        $user = User::where('user_name','like', '%'.$request->search.'%')->get(['id','user_avatar','user_name']);
        return  response()->json(['films' => $film, 'users' => $user],200);   
    }

    public function isLiked($id){
        $user = Auth::user();
        $film = Film::find($id);
        $is_liked = DB::table('likesfilms')->where('film_id', $id)->where('user_id', $user->id)->count()>0;
        return response()->json(['is_liked' => $is_liked],200);
    }

    public function reviewsByFilm($id){
        $reviews = Review::with(['user:user_name,id,user_avatar','film:id,title,film_cover'])->where('film_id',$id)->get();
        return response()->json($reviews,200);
    }

    public function randomFilms(){
        $ids = [rand(1,15)];
        while(count($ids) < 5){
            $id = rand(1,15);
            while(TRUE){
                if(in_array($id, $ids)){
                    $id = rand(1,15);
                }
                else{
                    array_push($ids, $id);
                    break;
                }
            }   
        }
        $films = Film::whereIn('id', $ids)->get(['id', 'title', 'director','film_cover']);
        return response()->json($films, 200);

        //$films = array_rand(Film::all->get()->toArray(), 5); Solução elegante
    }
}