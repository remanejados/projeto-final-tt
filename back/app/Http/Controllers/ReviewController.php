<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ReviewRequest;
use App\Models\Review;
use Auth;
use App\Models\User;
use App\Models\Comment;
use DB;

class ReviewController extends Controller
{
    //
    public function create(ReviewRequest $request)
    {

        $user = Auth::user();
        $review = new Review();
        $review->createReview($request,$user->id);
        return response()->json(['review'=> $review], 200);
    }
    public function update (ReviewRequest $request, $id){
        $review = Review::find($id);
        $review->updateReview($request);
        return response()->json(['review' => $review], 200);
    }
    public function index(){
        $reviews = Review::all();
        return response()->json(['review' => $reviews],200);
    }

    public function show($id){
        $review = Review::with(['user:id,user_name,user_avatar','film:id,title,film_cover'])->where('id', $id)->get();
        return response()->json($review, 200);
    }

    public function destroy($id){
        $review = Review::find($id);
        $review->delete();
        return response()->json(['Review deletado com sucesso'],200);
    }

    public function isLiked($id){
        $user = Auth::user();
        $review = Review::find($id);
        $is_liked = DB::table('likesreviews')->where('review_id', $id)->where('user_id', $user->id)->count()>0;
        return response()->json(['is_liked' => $is_liked],200);
    }

    public function commentsByReview($id){
        $comments = Comment::with(['user:user_name,id,user_avatar'])->where('review_id',$id)->get();
        return response()->json($comments,200);
    }
    public function nLikes($id){
        $review = Review::find($id);
        $n_likes = DB::table('likesreviews')->where('review_id', $id)->count();
        return response()->json(['n_likes' => $n_likes],200);
    }
}
