<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Film;
use App\Models\Review;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\UserResource;
// App\Http\Controllers\Auth;
use Auth;
use DB;

class UserController extends Controller
{
    //
    public function create(UserRequest $request)
    {
        $user = new User();
        $user->createUser($request);
        return response()->json(['user'=> $user], 200);
    }
    public function update (UserRequest $request){
        $user = Auth::user();
        $user->updateUser($request);
        return response()->json(['user' => $user], 200);
    }
    public function index(){
        $users = User::all();
        return response()->json(['user' => $users],200);
    }

    public function show($id){
        $user = User::where('id',$id)->get(['id','user_name','user_cover','user_avatar']);
        return response()->json(['user'=> $user], 200);
    }

    public function destroy(Request $request){
        $user = User::find($request->id);
        $user->delete();
        return response()->json(['Usuário deletado com sucesso'],200);
    }

    public function follow($id){
        $follow_user = Auth::user();
        $followed_user = User::find($id);
        if($followed_user->id != $follow_user->id){
            $is_following = DB::table('follows')->where('followed_id', $id)->where('follow_id', $follow_user->id)->count()>0;
            if($is_following){
                $follow_user->unfollowUser($id);
                return response()->json(['Deixou de seguir'],200);
            }
            else{
                $follow_user->followUser($id);
                return response()->json(['Começou a seguir'],200);
            }
        }
        return response()->json(['Não pode se seguir'],422);
    }

    public function likeReview($id){
        $user = Auth::user();
        $review = Review::find($id);
        $is_liking = DB::table('likesreviews')->where('review_id', $id)->where('user_id', $user->id)->count()>0;
            if($is_liking){
                $user->doUnlikeReview($id);
                return response()->json(['Deixou de curtir'],200);
            }
            else{
                $user->doLikeReview($id);
                return response()->json(['Passou a curtir'],200);
            }
    }

    public function likeFilm($id){
        $user = Auth::user();
        $film = Film::find($id);
        $is_liking = DB::table('likesfilms')->where('film_id', $id)->where('user_id', $user->id)->count()>0;
            if($is_liking){
                $user->doUnlikeFilm($id);
                return response()->json(['Deixou de curtir'],200);
            }
            else{
                $user->doLikeFilm($id);
                return response()->json(['Passou a curtir'],200);
            }
    }

    public function searchByName(Request $request){
        $user = DB::table('users')->where('user_name','like', '%'.$request->user_name.'%')->get();
        return  response()->json(['user' => $user],200);
    }

    public function friendList(){
        $user = Auth::user();
        $list = $user->follow;
        $edited_list = $list->map(function ($user) {
            return collect($user->toArray())
                ->only(['user_name', 'user_avatar', 'id'])
                ->all();
        });
        return  response()->json(['friend_list' => $edited_list],200);
    }

    public function likedFilms(){
        $user = Auth::user();
        $list = $user->like_film;
        $edited_list = $list->map(function ($user) {
            return collect($user->toArray())
                ->only(['title', 'film_cover', 'rating','resume', 'id'])
                ->all();
        });
        return  response()->json(['liked_films' => $edited_list],200);
    }

    public function likedReviews(){
        $user = Auth::user();
        $list = $user->likeReview;
        // $edited_list = $list->map(function ($user) {
        //     return collect($user->toArray())
        //         ->only(['title', 'film_cover', 'rating','resume', 'id'])
        //         ->all();
        // });
        return  response()->json(['liked_reviews' => $list],200);
    }

    public function followers(){
        $user = Auth::user();
        $list = $user->followed;
        $edited_list = $list->map(function ($user) {
            return collect($user->toArray())
                ->only(['user_name', 'user_avatar', 'id'])
                ->all();
        });
        return  response()->json(['followers' => $edited_list],200);
    }

    public function isFollowing($id){
        $follow_user = Auth::user();
        $followed_user = User::find($id);
        $is_following = DB::table('follows')->where('followed_id', $id)->where('follow_id', $follow_user->id)->count()>0;
        return response()->json(['is_following' => $is_following],200);
    }

    public function usersReviews($id){
        $user = User::find($id);
        $reviews = Review::where('user_id', $id)->with('film:id,film_cover,title','user:id,user_name,user_avatar')->get();
        return response()->json(['reviews' => $reviews],200);
    }
    public function usersComments($id){
        $user = User::find($id);
        $comments = $user->comments;
        return response()->json(['comments' => $comments],200);
    }

    public function feed(){
        $user = Auth::user();
        $list = $user->follow;
        $edited_list = $list->pluck('id');
        $reviews = Review::whereIn('user_id', $edited_list)->orderBy('updated_at', 'DESC')->with('user:id,user_name,user_avatar','film:id,title,film_cover')->get();
        return response()->json(['feed' => $reviews], 200);
    }

    public function getDetails(){
        $user = Auth::user();
        return response()->json(['user' => $user],200);
    }

    public function lastLikes($id){
        $user = User::find($id);
        $films_likes = $user->likeFilm->reverse()->take(2);
        $reviews_likes = $user->likeReview->reverse()->take(2);
        //$likes = $films_likes->concat($reviews_likes);
        return response()->json(['films' => $films_likes, 'reviews' => $reviews_likes], 200);
        //Film::whereIn('film_id', $likes)
    }

    public function avgRating($id){
        $user = User::find($id);
        $rate = $user->likeReview->pluck('film_rating')->toArray();
        $average = number_format(array_sum($rate)/count($rate),1);
        return response()->json($average, 200);
    }
}
